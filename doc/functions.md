Available Functions
===================

This library provide only MariaDB compatible functions.

## Functions

### IF

Allow to use [`IF()`](https://mariadb.com/kb/en/if-function/) from mariaDB. For now, the function do not support any parameter.

```php
<?php
// Usage example

class FooRepository extends EntityRepository
{
    public function findOneRandomly(): array
    {
        return $this->createQueryBuilder('f')
            ->addSelect('(IF(f.enabled, "Enabled", "Disabled")) AS enabled_text')
            ->getQuery()
            ->getResult()
        ;
    }
}

```

### RAND\(\)

Allow to use [`RAND()`](https://mariadb.com/kb/en/rand/) from mariaDB. For now, the function do not support any parameter.

```php
<?php
// Usage example

class FooRepository extends EntityRepository
{
    public function findOneRandomly(): ?Foo
    {
        return $this->createQueryBuilder('f')
            ->orderBy('RAND()')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}

```

### MATCH AGAINST

Allow to use [`MATCH AGAINST()`](https://mariadb.com/kb/en/match-against/) from mariaDB. It will allow you to calculate
a score and order you query with it.

```php
<?php
// Usage examples

class FooRepository extends EntityRepository
{
    public function findByTitleMatch(string $search): array
    {
        return $this->createQueryBuilder('f')
            ->addSelect('(MATCH_AGAINST(f.title, :text)) AS HIDDEN score')
            ->having('score > 0')
            ->orderBy('score', 'DESC')
            ->setParameter('text', $search)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findSearch(string $search): array
    {
        $matchAgainst = [
            'MATCH_AGAINST(f.title, :text)*2',
            'MATCH_AGAINST(f.description, :text)',
        ];

        return $this->createQueryBuilder('f')
            ->addSelect(sprintf('(%s) AS HIDDEN score', implode('+', $matchAgainst)))
            ->having('score > 0')
            ->orderBy('score', 'DESC')
            ->setParameter('text', $search)
            ->getQuery()
            ->getResult()
        ;
    }
}
```


## Usage with Symfony

### Config references

You just have to register the functions to your doctrine connection config.

```yaml
doctrine:
    #...
    orm:
        #...
        dql:
            string_functions:
                MATCH_AGAINST: WS\Library\DoctrineExtensions\Functions\MariaDB\MatchAgainstFunction
                RAND: WS\Library\DoctrineExtensions\Functions\MariaDB\RandFunction
    #...

```

If you have more than one doctrine connection, use this config:

```yaml
doctrine:
    #...
    orm:
        entity_managers:
            another_connection: #...
            default:
                #...
                dql:
                    string_functions:
                        MATCH_AGAINST: WS\Library\DoctrineExtensions\Functions\MariaDB\MatchAgainstFunction
                        RAND: WS\Library\DoctrineExtensions\Functions\MariaDB\RandFunction
    #...

```

You can find more doc on [Symfony official site](https://symfony.com/doc/current/doctrine/custom_dql_functions.html).
