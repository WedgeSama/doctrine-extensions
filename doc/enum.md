Doctrine Enum type for native PHP Enum
======================================

This library provide a new Doctrine ORM type for native PHP enum.

## Create native PHP Enum

```php
<?php

namespace App\Entity;

enum FooBarEnum: string
{
    private const FOO = 'foo';
    private const BAR = 'bar';
}
```

## Project without Symfony

### Create Doctrine Enum type

```php
<?php

namespace App\Doctrine\DBAL\Types;

use App\Entity\FooBarEnum;
use WS\Library\DoctrineExtensions\DBAL\Types\AbstractEnumType;

class FooBarEnumType extends AbstractEnumType
{
    protected function getEnumClass(): string
    {
        return FooBarEnum::class;
    }

    public function getName(): string
    {
        return 'foo_bar_enum';
    }
}
```

### Register Doctrine Enum type

```php
<?php

use App\Doctrine\DBAL\Types\FooBarEnumType;

\Doctrine\DBAL\Types\Type::addType('foo_bar_enum', FooBarEnumType::class);
```

## Project with Symfony

### Use maker to generate EnumType

```bash
php bin/console make:doctrine:enum-type
```

It will create a new class in `src/Doctrine/DBAL/Types` directory and register it in `config/packages/doctrine.yaml`.

### Use EnumType

```php
<?php

namespace App\Entity;

use App\Doctrine\DBAL\Types\FooBarEnumType;

#[ORM\Entity]
class Foo
{
    #[ORM\Column(type: 'foo_bar_enum')]
    private FooBarEnum $fooBar;
    
    public function getFooBar(): FooBarEnum
    {
        return $this->fooBar;
    }
    
    public  function setFooBar(FooBarEnum $fooBar): static 
    {
        $this->fooBar = $fooBar;
        
        return $this;
    }
}
```
