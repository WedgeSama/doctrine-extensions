Doctrine Extensions
===================

A library to extend [Doctrine ORM](https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/index.html)
capabilities.

## Install

```
composer require wedgesama/doctrine-extensions
```

### Symfony integration (^4.4|^5.0)

Flex recipe not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    WS\Library\DoctrineExtensions\Bridge\Symfony\WSDoctrineExtensionsBundle::class => ['all' => true],
    //...
];
```

- Create a new `config\packages\ws_doctrine_extensions.yaml` file, you will config the bundle here.
```yaml
ws_doctrine_extensions: ~
```

- That all for default configuration.

## Documentations

Here all capabilities documentations:
- [Available functions](doc/functions.md): Provide some useful Doctrine ORM functions for mariaDB.
- [Enum types](doc/enum.md): Provide a new Doctrine ORM type for native PHP enum.
- Auto-prefix table names (DOC TODO)

## TODO

- Better documentation.
- Flex recipe for Symfony.
- Add example for standalone usage.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
