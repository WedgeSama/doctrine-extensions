<?php

/*
 * This file is part of the wedgesama/doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Id;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use DrosalysWeb\StringExtensions\Random\ConstantSizeTokenGenerator;

/**
 * Class ConstantSizeIdGenerator
 *
 * @author Benjamin Georgeault
 */
class ConstantSizeIdGenerator extends AbstractIdGenerator
{
    private ConstantSizeTokenGenerator $tokenGenerator;

    public function __construct()
    {
        $this->tokenGenerator = new ConstantSizeTokenGenerator();
    }

    /**
     * @param null|object $entity
     * @throws \Exception
     */
    public function generate(EntityManager $em, $entity): string
    {
        $id = $this->tokenGenerator->generateToken();

        if (null !== $entity && null !== $em->getRepository(get_class($entity))->findOneBy(['id' => $id])) {
            $id = $this->generate($em, $entity);
        }

        return $id;
    }
}
