<?php
/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Functions\MariaDB;

use Doctrine\ORM\Query\AST\ArithmeticExpression;
use Doctrine\ORM\Query\AST\ConditionalExpression;
use Doctrine\ORM\Query\AST\ConditionalFactor;
use Doctrine\ORM\Query\AST\ConditionalPrimary;
use Doctrine\ORM\Query\AST\ConditionalTerm;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class IfFunction
 *
 * @author Benjamin Georgeault
 */
class IfFunction extends FunctionNode
{
    private ConditionalExpression|ConditionalFactor|ConditionalPrimary|ConditionalTerm $condition;
    private ?ArithmeticExpression $yes;
    private ?ArithmeticExpression $no;

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->condition = $parser->ConditionalExpression();
        $parser->match(Lexer::T_COMMA);
        $this->yes = $this->parseValue($parser);
        $parser->match(Lexer::T_COMMA);
        $this->no = $this->parseValue($parser);

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker): string
    {
        return sprintf(
            'IF(%s, %s, %s)',
            $sqlWalker->walkConditionalExpression($this->condition),
            $this->walkValue($sqlWalker, $this->yes),
            $this->walkValue($sqlWalker, $this->no),
        );
    }

    private function parseValue(Parser $parser): ?ArithmeticExpression
    {
        if ($parser->getLexer()->isNextToken(Lexer::T_NULL)) {
            $parser->match(Lexer::T_NULL);
            return null;
        }

        return $parser->ArithmeticExpression();
    }

    private function walkValue(SqlWalker $sqlWalker, ?ArithmeticExpression $value): string
    {
        if (null === $value) {
            return 'NULL';
        }

        return $sqlWalker->walkArithmeticPrimary($value);
    }
}
