<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Functions\MariaDB;

use Doctrine\ORM\Query\AST;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;


/**
 * Class MatchAgainstFunction
 *
 * @author Benjamin Georgeault
 */
class MatchAgainstFunction extends FunctionNode
{
    /**
     * @var AST\PathExpression[]
     */
    public array $columns = [];

    /**
     * @var AST\InputParameter|AST\ArithmeticExpression
     */
    public AST\Node $needle;

    public ?AST\Literal $mode = null;

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        do {
            $this->columns[] = $parser->StateFieldPathExpression();
            $parser->match(Lexer::T_COMMA);
        } while ($parser->getLexer()->isNextToken(Lexer::T_IDENTIFIER));

        $this->needle = $parser->InParameter();

        while ($parser->getLexer()->isNextToken(Lexer::T_STRING)) {
            $this->mode = $parser->Literal();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker): string
    {
        $sqlColumns = array_map(function (AST\PathExpression $column) use ($sqlWalker) {
            return $column->dispatch($sqlWalker);
        }, $this->columns);

        return sprintf(
            'MATCH(%s) AGAINST (%s%s)',
            implode(', ', $sqlColumns),
            $this->needle->dispatch($sqlWalker),
            $this->mode ? ' '.$this->mode->dispatch($sqlWalker) : ''
        );
    }
}
