<?php

/*
 * This file is part of the wedgesama/doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/**
 * Class TablePrefixSubscriber
 *
 * @author Benjamin Georgeault
 */
class TablePrefixSubscriber implements EventSubscriber
{
    private string $prefix;

    /**
     * @var string[]
     */
    private array $namespaces;

    /**
     * TablePrefixSubscriber constructor.
     * @param string[] $namespaces
     */
    public function __construct(string $prefix, array $namespaces)
    {
        $this->prefix = $prefix;
        $this->namespaces = $namespaces;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isInheritanceTypeSingleTable() && !$metadata->isRootEntity()) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if (!$this->support($reflectionClass->getNamespaceName())) {
            return;
        }

        $metadata->setPrimaryTable([
            'name' => $this->prefix.$metadata->getTableName(),
        ]);

        foreach ($metadata->getAssociationMappings() as $fieldName => $mapping) {
            if (
                $mapping['type'] == ClassMetadataInfo::MANY_TO_MANY &&
                array_key_exists('name', $metadata->associationMappings[$fieldName]['joinTable'])
            ) {
                $metadata->associationMappings[$fieldName]['joinTable']['name'] =
                    $this->prefix.$metadata->associationMappings[$fieldName]['joinTable']['name']
                ;
            }
        }
    }

    private function support(string $class): bool
    {
        foreach ($this->namespaces as $namespace) {
            if (false !== strstr($class, $namespace)) {
                return true;
            }
        }

        return false;
    }
}
