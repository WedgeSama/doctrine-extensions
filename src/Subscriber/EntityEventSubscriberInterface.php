<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Subscriber;

/**
 * Interface EntityEventSubscriberInterface
 *
 * @author Benjamin Georgeault
 *
 * @see \Doctrine\Common\EventSubscriber
 */
interface EntityEventSubscriberInterface
{
    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public static function getSubscribedEvents(): array;

    /**
     * Return the full-qualify classname the subscribe will be triggered.
     *
     * @return string
     */
    public static function getEntityClass(): string;
}
