<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/**
 * Class TableNamespaceNamingSubscriber
 *
 * @author Benjamin Georgeault
 */
class TableNamespaceNamingSubscriber implements EventSubscriber
{
    /**
     * @var string[]
     */
    private array $namespaces;

    private Inflector $inflector;

    /**
     * TableNamespaceNamingSubscriber constructor.
     * @param string[] $namespaces
     */
    public function __construct(array $namespaces)
    {
        $this->namespaces = array_map(function (string $namespace) {
            return rtrim($namespace, '\\');
        }, $namespaces);

        $this->inflector = InflectorFactory::create()->build();
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isInheritanceTypeSingleTable() && !$metadata->isRootEntity()) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        $namespaceName = $reflectionClass->getNamespaceName();

        if (null === $prefixNamespace = $this->getPrefixNamespace($namespaceName)) {
            return;
        }

        if ($namespaceName === $prefixNamespace) {
            return;
        }

        $prefixOrigin = trim(preg_replace('/^'.preg_quote($prefixNamespace).'/', '', $namespaceName), '\\');
        $prefixArray = array_map([$this->inflector, 'tableize'], explode('\\', $prefixOrigin));
        $prefix = implode('_', $prefixArray);

        $metadata->setPrimaryTable([
            'name' => $prefix.'_'.$metadata->getTableName(),
        ]);

        foreach ($metadata->getAssociationMappings() as $fieldName => $mapping) {
            if (
                $mapping['type'] == ClassMetadataInfo::MANY_TO_MANY &&
                array_key_exists('name', $metadata->associationMappings[$fieldName]['joinTable'])
            ) {
                $metadata->associationMappings[$fieldName]['joinTable']['name'] =
                    $prefix.$metadata->associationMappings[$fieldName]['joinTable']['name']
                ;
            }
        }
    }

    private function getPrefixNamespace(string $namespaceName): ?string
    {
        foreach ($this->namespaces as $namespace) {
            if (false !== strstr($namespaceName, $namespace)) {
                return $namespace;
            }
        }

        return null;
    }
}
