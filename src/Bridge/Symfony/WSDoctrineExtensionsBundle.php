<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony;

use WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection\Compiler\EntityEventSubscriberPass;
use WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection\Compiler\SimplePrefixPass;
use WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection\WSDoctrineExtensionsExtension;
use WS\Library\DoctrineExtensions\Subscriber\EntityEventSubscriberInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WSDoctrineExtensionsBundle
 *
 * @author Benjamin Georgeault
 */
class WSDoctrineExtensionsBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new SimplePrefixPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 255);
        $container->addCompilerPass(new EntityEventSubscriberPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 255);

        $container->registerForAutoconfiguration(EntityEventSubscriberInterface::class)
            ->addTag('ws.doctrine.orm.entity_subscriber')
        ;
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new WSDoctrineExtensionsExtension();
    }
}
