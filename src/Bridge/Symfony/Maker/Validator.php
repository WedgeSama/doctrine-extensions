<?php
/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony\Maker;

use Symfony\Bundle\MakerBundle\Exception\RuntimeCommandException;

/**
 * Class Validator
 *
 * @author Benjamin Georgeault
 */
final class Validator
{
    public static function notBlank(string $value = null): string
    {
        if (null === $value || '' === $value) {
            throw new RuntimeCommandException('This value cannot be blank.');
        }

        return $value;
    }

    public static function enumExists(string $enumName, string $errorMessage = ''): string
    {
        self::notBlank($enumName);

        if (!enum_exists($enumName)) {
            $errorMessage = $errorMessage ?: sprintf('Enum "%s" doesn\'t exist; please enter an existing full enum name.', $enumName);

            throw new RuntimeCommandException($errorMessage);
        }

        return $enumName;
    }
}
