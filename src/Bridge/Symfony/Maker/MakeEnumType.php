<?php
/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony\Maker;

use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\FileManager;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Util\UseStatementGenerator;
use Symfony\Bundle\MakerBundle\Util\YamlSourceManipulator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;
use WS\Library\DoctrineExtensions\Enums\AbstractEnumType;

/**
 * Class MakeEnumType
 *
 * @author Benjamin Georgeault
 */
final class MakeEnumType extends AbstractMaker
{
    private string $enumTypeName;

    public function __construct(
        private FileManager $fileManager,
    ) {}

    public static function getCommandName(): string
    {
        return 'make:doctrine:enum-type';
    }

    public static function getCommandDescription(): string
    {
        return 'Creates a new doctrine enum type';
    }

    public function configureCommand(Command $command, InputConfiguration $inputConfig): void
    {
        $command
            ->addArgument('enum-name', InputArgument::OPTIONAL, 'The name of the PHP enum')
        ;

        $inputConfig->setArgumentAsNonInteractive('enum-name');
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command): void
    {
        if (null === $enumName = $input->getArgument('enum-name')) {
            $argument = $command->getDefinition()->getArgument('enum-name');

            $question = new Question($argument->getDescription());
            $enumName = $io->askQuestion($question);

            $input->setArgument('enum-name', $enumName);
        }

        if (!enum_exists($enumName)) {
            $enumName = 'App\\Entity\\' . $enumName;
            $input->setArgument('enum-name', $enumName);
        }

        $enumTypeName = preg_replace('/^App\\\\(Entity\\\\)?/', 'App\\Doctrine\\DBAL\\EnumType\\', $enumName);

        $defaultEnumTypeName = Str::asClassName(
            sprintf('%s EnumType', preg_replace('/Enum$/', '', $enumTypeName))
        );

        $this->enumTypeName = $io->ask(
            'Choose a name for your doctrine Enum type',
            $defaultEnumTypeName,
        );
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator): void
    {
        $enum = Validator::enumExists($input->getArgument('enum-name'));

        $enumDetails = $generator->createClassNameDetails(
            $enum,
            'Entity\\',
        );

        $enumTypeDetails = $generator->createClassNameDetails(
            preg_replace('/^App\\\\/', '', $this->enumTypeName),
            '',
            'EnumType',
        );

        $useStatements = new UseStatementGenerator([
            AbstractEnumType::class,
            $enum,
        ]);

        $generator->generateClass(
            $enumTypeDetails->getFullName(),
            __DIR__.'/../Resources/skeleton/enumType/EnumType.tpl.php',
            [
                'use_statements' => $useStatements,
                'enum_name' => $enumDetails->getShortName(),
                'doctrine_type_name' => $doctrineName = Str::asSnakeCase(
                    preg_replace('/Type$/', '', $enumTypeDetails->getShortName()),
                ),
            ],
        );

        $doctrineYamlUpdated = $this->updateDoctrineYamlConfig($generator, $doctrineName, $enumTypeDetails->getFullName());

        $generator->writeChanges();

        $this->writeSuccessMessage($io);

        $nextSteps = [
            sprintf('Review your new <info>%s</info> class.', $enumTypeDetails->getFullName()),
        ];

        if (!$doctrineYamlUpdated) {
            $nextSteps[] = "Your <info>doctrine.yaml</info> could not be updated automatically. You'll need to add the enum type manually.";
        }

        $nextSteps = array_map(static fn ($step) => sprintf('  - %s', $step), $nextSteps);
        $io->text($nextSteps);
    }

    public function configureDependencies(DependencyBuilder $dependencies)
    {
    }

    private function updateDoctrineYamlConfig(Generator $generator, string $doctrineName, string $enum): bool
    {
        $path = 'config/packages/doctrine.yaml';

        $manipulator = new YamlSourceManipulator(
            $this->fileManager->getFileContents($path),
        );

        $data = $manipulator->getData();

        // TODO handle multiple connections.

        if (!array_key_exists('types', $data['doctrine']['dbal'])) {
            $data['doctrine']['dbal']['types'] = [];
        }

        if (array_key_exists($doctrineName, $data['doctrine']['dbal']['types'])) {
            return false;
        }

        $data['doctrine']['dbal']['types'][$doctrineName] = $enum;

        $manipulator->setData($data);

        $generator->dumpFile($path, $manipulator->getContents());

        return true;
    }
}
