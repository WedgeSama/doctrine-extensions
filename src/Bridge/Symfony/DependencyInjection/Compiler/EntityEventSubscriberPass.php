<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection\Compiler;

use WS\Library\DoctrineExtensions\Subscriber\EntityEventSubscriberInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;

/**
 * Class EntityEventSubscriberPass
 *
 * @author Benjamin Georgeault
 */
class EntityEventSubscriberPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    private const TAG_NAME = 'ws.doctrine.orm.entity_subscriber';

    public function process(ContainerBuilder $container): void
    {
        $subscribers = $this->findAndSortTaggedServices(self::TAG_NAME, $container);

        foreach ($subscribers as $reference) {
            $id = $reference->__toString();
            $definition = $container->getDefinition($id);
            $class = $definition->getClass() ?? $id;

            try {
                $ref = new \ReflectionClass($class);
            } catch (\ReflectionException $e) {
                throw new InvalidArgumentException(sprintf(
                    'Cannot found class "%s" for the service "%s".',
                    $class,
                    $id
                ));
            }
            
            if (!$ref->implementsInterface(EntityEventSubscriberInterface::class)) {
                throw new LogicException(sprintf(
                    'The class "%s" used by the service "%s" tagged "%s" must implement "%s" interface.',
                    $class,
                    $id,
                    self::TAG_NAME,
                    EntityEventSubscriberInterface::class
                ));
            }

            if (!class_exists($entityClass = call_user_func([$class, 'getEntityClass']))) {
                throw new InvalidArgumentException(sprintf(
                    'Entity class "%s" for the service "%s" not found.',
                    $entityClass,
                    $class
                ));
            }

            $events = call_user_func([$class, 'getSubscribedEvents']);

            if (empty($events)) {
                continue;
            }

            foreach ($definition->getTag(self::TAG_NAME) as $attributes) {
                $forwardAttributes = [
                    'entity' => $entityClass,
                ];

                foreach (['lazy', 'entity_manager'] as $key) {
                    if (array_key_exists($key, $attributes)) {
                        $forwardAttributes[$key] = $attributes[$key];
                    }
                }

                foreach ($events as $event) {
                    $definition->addTag('doctrine.orm.entity_listener', array_merge($forwardAttributes, [
                        'event' => $event,
                    ]));
                }
            }
        }
    }
}
