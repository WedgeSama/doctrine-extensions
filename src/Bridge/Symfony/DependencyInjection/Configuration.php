<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('ws_doctrine_extensions');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->tablePrefixNode())
            ->end()
        ;

        return $treeBuilder;
    }

    private function tablePrefixNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('table_prefix');

        return $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->arrayNode('simple_prefix')
                    ->useAttributeAsKey('prefix')
                    ->arrayPrototype()
                        ->beforeNormalization()->castToArray()->end()
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
                ->arrayNode('namespace_naming')
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;
    }
}
