<?php

/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Bridge\Symfony\DependencyInjection;

use Symfony\Bundle\MakerBundle\MakerBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use WS\Library\DoctrineExtensions\Subscriber\TableNamespaceNamingSubscriber;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * Class WSDoctrineExtensionsExtension
 *
 * @author Benjamin Georgeault
 */
class WSDoctrineExtensionsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        if (class_exists(MakerBundle::class)) {
            $loader->load('maker.yaml');
        }

        if ($config['table_prefix']['enabled']) {
            if (!empty($config['table_prefix']['namespace_naming'])) {
                $definition = new Definition(TableNamespaceNamingSubscriber::class, [
                    $config['table_prefix']['namespace_naming']
                ]);

                $definition
                    ->setPublic(false)
                    ->addTag('doctrine.event_subscriber')
                ;

                $container->setDefinition(TableNamespaceNamingSubscriber::class, $definition);
            }

            if (!empty($prefixes = $config['table_prefix']['simple_prefix'])) {
                $container->setParameter('ws_doctrine_extensions.table_prefix.simple_prefix.namespaces', $prefixes);
            }
        }
    }

    public function getAlias(): string
    {
        return 'ws_doctrine_extensions';
    }
}
