<?= "<?php\n" ?>

namespace <?= $namespace ?>;

<?= $use_statements; ?>

class <?= $class_name ?> extends AbstractEnumType
{
    protected function getEnum(): string
    {
        return <?= $enum_name ?>::class;
    }

    public function getName(): string
    {
        return '<?= $doctrine_type_name ?>';
    }
}
