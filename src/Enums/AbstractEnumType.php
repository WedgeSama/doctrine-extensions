<?php
/*
 * This file is part of the doctrine-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\DoctrineExtensions\Enums;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class AbstractEnumType
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractEnumType extends Type
{
    /**
     * @return class-string
     */
    protected abstract function getEnum(): string;

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        /** @var \BackedEnum $enum */
        $enum = $this->getEnum();

        return sprintf('ENUM(%s)', implode(', ', array_map(
            fn ($item) => sprintf("'%s'", $item->value),
            $enum::cases(),
        )));
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): \BackedEnum
    {
        /** @var \BackedEnum $enum */
        $enum = $this->getEnum();

        return $enum::from($value);
    }

    /**
     * @param \BackedEnum $value
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        return $value->value;
    }
}
